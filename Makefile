# This is the makefile for the greedy algorithm selection
# Written by Yuzong Liu
#
CC=g++ -std=c++11
CFLAGS=-Wall
OPTIM=-O3
OBJECTS=$(SOURCES:.cc=.o)
LIBS=-lm
TAR=/bin/tar

all: greedy_submod_select_basic greedy_submod_select_diversity

greedy_submod_select_basic:
	$(CC) $(CFLAGS) $(OPTIM) greedy_submod_select_basic.cc arguments.cc error.cc -o greedy_submod_select_basic $(LIBS)

greedy_submod_select_diversity:
	$(CC) $(CFLAGS) $(OPTIM) greedy_submod_select_diversity.cc arguments.cc error.cc -o greedy_submod_select_diversity $(LIBS)

clean:
	rm -f *.o greedy_submod_select_basic greedy_submod_select_diversity

dist:
	$(TAR) cvzf ../modified_greedy_select-`date +%b_%d_%Y | sed -e 's, ,,g'`.tar.gz *.{cc,h} Makefile README
