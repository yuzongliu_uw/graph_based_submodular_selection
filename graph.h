/*
 * graph.h
 *
 *  Created on: Jan 27, 2013
 *      Author: Yuzong Liu (yzliu@ee.washington.edu)
 *
 *  Implementation for (sparse) graph data structure
 *
 */
#ifndef GRAPH_H_
#define GRAPH_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include "stdio.h"
#include "stdlib.h"

using namespace std;

/////////////////////////////////////////////////////////////////////
// structure of graph representation
/////////////////////////////////////////////////////////////////////

typedef unordered_map<unsigned int, float> fastMap;

struct node {

	unsigned short NN;		// number of nearest neighbors
	unsigned int index;		// index of the node
	unsigned int *idx;		// list of indices of NNs
	fastMap wgtMap;

    void setWeight(unsigned int id, float wgt) {
        wgtMap[id] = wgt;
    }

	float getWeight(unsigned int id) {
		return wgtMap[id];
	}
};

/////////////////////////////////////////////////////////////////////
// to init the nodes in the graph with data from the original graph
// format of the sparse graph:
// <total number of vertices in the graph (unsigned int)>
// <index of vertex #1 (unsigned int)> <number of nearest neighbors (N) (unsigned short)> <index of nn #1 (unsigned int)> <weight of nn #1 (float)> ... <index of nn #N (unsigned int)> <weight of nn #N (float)>
// ... ...
// <index of vertex #M (unsigned int)> <number of nearest neighbors (N) (unsigned short)> <index of nn #1 (unsigned int)> <weight of nn #1 (float)> ... <index of nn #N (unsigned int)> <weight of nn #N (float)>
/////////////////////////////////////////////////////////////////////

void readGraph(node *graph, const char* graphFileName, int verbosity)
{
	ifstream iFile;
	printf("Reading File %s\t", graphFileName);
	fflush(stdout);
	iFile.open(graphFileName, ios::in|ios::binary);
	if (!iFile.is_open()) {
	  printf("ERROR: cannot open file %s",graphFileName);
	}


	// Skip first unsigned int as it contains number of vertices in the
	// graph.
	unsigned int numNodesInGraph;
	iFile.read((char*)&numNodesInGraph, sizeof(unsigned int));

	unsigned int ii = 0;	// count of how many rows being read
	node *ptr = graph;		// get pointer to the graph

	while (!iFile.eof()) {

		unsigned int vertex_id = 0;
		unsigned int numNeighbors = 0;

		iFile.read((char*)&vertex_id, sizeof(unsigned int));
		iFile.read((char*)&numNeighbors, sizeof(unsigned short));

		// if the vertex has no neighbors, then simply got to the next vertex.
		if (numNeighbors == 0)
		  continue;

		// if the numNeigbors != 0 and file ends abruptly, get out of the loop.
		if (iFile.eof())
		  break;

		// get current node information
		ptr->index = vertex_id;
		ptr->NN = numNeighbors;
		ptr->idx = new unsigned int[numNeighbors];
		// ptr->weight = new float[numNeighbors];

		for (unsigned short i = 0; i < numNeighbors; i++) {
			unsigned int t1;
		  	float t2;
		  	iFile.read((char*)&t1, sizeof(unsigned int));
		  	iFile.read((char*)&t2, sizeof(float));
		  	ptr->idx[i] = t1;
		  	ptr->setWeight(t1, t2);
		  	// Sanity check.
		  	if ((ptr->getWeight(t1) < 0)&&(verbosity>=5)) {
				printf("Fatal problem with graph: negative weights in the graph");
				exit(-1);
		  	}
		}
		graph[vertex_id].setWeight(vertex_id, 1.0);
		++ii; ++ptr;

		if ( !(ii % 1000) ) {
	  	  	fprintf(stderr,".");
			fflush(stderr);
		}
	}

	iFile.close();
	fprintf(stderr,"\n");
}

/////////////////////////////////////////////////////////////////////
// to read the number of vertices in the graph.
/////////////////////////////////////////////////////////////////////
unsigned int getNumNodes(const char* graphFileName) {
	ifstream iFile;
	fflush(stdout);
	iFile.open(graphFileName, ios::in|ios::binary);
	if (!iFile.is_open()) {
	  printf("ERROR: cannot open file %s",graphFileName);
	}
  	// read the number of vertices.
	unsigned int  nVertices;
  	iFile.read((char*)&nVertices, sizeof(unsigned int));
	iFile.close();
  	return nVertices;
}

/////////////////////////////////////////////////////////////////
// a debug routine that given idx, returns information
// about that node in the graph.
// Information includes, number of NN's, idx & weights of NN's
/////////////////////////////////////////////////////////////////////
void lookAtNode(node *graph, unsigned int idx) {

  cout << "Node #" << idx << endl;
  cout << "Positions -- " << graph[idx].index  << endl;
  for (unsigned int i = 0; i < graph[idx].NN; i++) {
    cout << "NN_idx = " << graph[idx].idx[i] << " NN_weight = "
	 	 << graph[idx].getWeight(graph[idx].idx[i]) << endl;
  }

}


#endif	/* GRAPH_H_ */
