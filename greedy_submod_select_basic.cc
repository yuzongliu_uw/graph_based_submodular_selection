/*
 *  Latest modified on: Dec 2, 2015
 *      Author: Yuzong Liu (yzliu@uw.edu)
 *
 *	1. The code implements subset selection using submodular functions
 *  2. The functions include: facility location function, saturate coverage function, softmax function, and diversity reward function
 *  3. Modified greedy algorithm with cost constraints
 *	4. Speed up with accelerated greedy algorithm (Minoux 1976)
 *  5. allows a mixture of clustering algorithms
 *  6. allows a combination of both coverage function and diversity function
 *  7. allows no diversity term (for the purpose of fast tuning)
 *  8. implementation of sparse similarity matrix to save memory (version 1.1)
 *
 *
 */

#include <queue>
#include "arguments.h"
#include "error.h"
#include "submodularFunction.h"
#include "graph.h"
#include "helper.h"


char* strInputMat=NULL;
double per = 0;
unsigned int nUtt = 0;
const char* obj="facility";
char* strOutput=NULL;
char* costFile=NULL;
char* help=NULL;
int verb=0;
double alpha=0;
double beta=0.8;
double gamma0=1;
bool isList=false;

Arg Arg::Args[]={

        Arg("graph", Arg::Req, strInputMat, "the sparse graph filelist in binary format",Arg::SINGLE),
        Arg("n", Arg::Req, nUtt, "the total number of senences/features",Arg::SINGLE),
		Arg("per", Arg::Req, per, "the percentage of data to be selected",Arg::SINGLE),
        Arg("out", Arg::Req, strOutput, "the output file",Arg::SINGLE),
		Arg("obj", Arg::Opt, obj, "objective function in the coverage term, options={facility,saturate,softmax}",Arg::SINGLE),
		Arg("cost", Arg::Opt, costFile, "the cost file", Arg::SINGLE),
		Arg("alpha",Arg::Opt, alpha, "regularizer of cardinality", Arg::SINGLE),
		Arg("beta",Arg::Opt, beta, "the trade-off parameter when using coverage obj w/ saturation", Arg::SINGLE),
		Arg("gamma",Arg::Opt, gamma0, "parameter of the softmax function", Arg::SINGLE),
		Arg("isList",Arg::Opt, isList, "whether the input is a list or a single graph", Arg::SINGLE),
        Arg("verb", Arg::Opt, verb, "verbosity",Arg::SINGLE),
        Arg("help", Arg::Help, help, "Print this message"),
        Arg()
};


int main(int argc, char** argv){

    ////////////////////////////////////////////
    struct rusage rus; /* starting time */
    struct rusage rue; /* ending time */

    // get start time.
    getrusage(RUSAGE_SELF,&rus);

	bool parse_was_ok = Arg::parse(argc,(char**)argv);
        if(!parse_was_ok){
          Arg::usage(); exit(-1);
        }

	unsigned int i,j;
	double tmpd;
	double gamma = gamma0;
	int verbosity = verb;
	ifstream iFile;


	// *********************************************************
	// read the list of costs for knapsack constraint
	// *********************************************************
	vector<double> costList;
	double totalCost = 0;
	printlines();
	printf("Reading list of costs from %s...\n", costFile);
	iFile.open(costFile, ios::in);
	if (!iFile.is_open()){
		printf("Use unit cost for each utterance\n");
		for (i = 0; i < nUtt; i++) {
			costList.push_back(1.0);
			totalCost += 1.0;
		}
	}
	else {
		for (i = 0; i < nUtt; i++) {
			iFile >> tmpd;
			totalCost += tmpd;
			costList.push_back(tmpd);
		}
	}
	iFile.close();

	double maxCost = totalCost*(per/100);
	printf("Total cost of the dataset is %f.\n", totalCost);
	printf("Maximum budget for the subset is %f.\n", maxCost);

	// *********************************************************
	// read the list of distributed graphs
	// *********************************************************
	vector<string> graphList;
	string tmp;

	printlines();
	if (isList)
	{
		printf("Reading list of graph files from %s...\n", strInputMat);
		iFile.open(strInputMat, ios::in);
		if (!iFile.is_open()){
			printf("ERROR: cannot open file %s",strInputMat);
		}
		while (!iFile.eof()) {
			getline(iFile, tmp, '\n');
			if (tmp.length() > 0)
				graphList.push_back(tmp);
		}
		iFile.close();
	}
	else
	{
		printf("Reading graph file from %s...\n", strInputMat);
		tmp = strInputMat;
		graphList.push_back(tmp);
	}

	// *********************************************************
    // initialize affinity matrix
	// *********************************************************
	unsigned int nV = nUtt ;
	unsigned int nRow = 0;
	unsigned int numNodesInGraph = 0; // row count

	// FILE* fp;

	printlines();

	node *graph = new node[nUtt]; // allocate memory for graph
	// read individual graph now...
	for (unsigned int m = 0; m < graphList.size(); m++) {
		printf("Loading graph from %s...\n",graphList[m].c_str());
		readGraph(graph, graphList[m].c_str(), verbosity);
		nRow = getNumNodes(graphList[m].c_str());
		numNodesInGraph += nRow;
		printf("File %s has %d vertices.\n", graphList[m].c_str(),nRow);
		// fclose(fp);
	}
    printf("Done...\n");

	// sanity check, if the total number of rows is correct
	if (numNodesInGraph != nV) {
		cout << "Something is wrong! Invalid graph\n";
		cout << "# Rows = " << numNodesInGraph << "\t # Columns = " << nV << endl;
		exit(-1);
	}
	printf("Sanity check passed...\n");

	// *********************************************************
	// General Setups
	// *********************************************************

	unsigned int nSelected = 0 ; // number of utterances selected
	int* SelectedSet = new int[nV]; // array to store the selected utt ids
	bool* hashSelected = new bool[nV]; // an index table to show whether an utt is selected
	for(i = 0;i < nV;i++) hashSelected[i]=false;


	double* preCompute = new double[nV]; //for speed-up	in the coverage term
    for (i = 0; i < nV; i++) { preCompute[i] = 0; }
    double maxV = 0;
	double sumV = 0; // store the summation of coverage term and diversity term
    double preV = 0;
    double objV = 0;
	double currentCost = 0;

	double* threshV = new double[nV]; // thresholds for saturation
    for (i = 0; i < nV; i++) {
        threshV[i] = 0;
        for (j = 0; j < graph[i].NN; j++) {		// for node i in the graph
            threshV[i]+=graph[i].getWeight(graph[i].idx[j]);		// accumulate the degree of node i
        }
    }

	priority_queue <Increment> rho;
	int sort_cnt = 0;
	FILE* ofp = fopen(strOutput,"w");
	int topid;
	double newV;

	// *********************************************************
	// if the diversity term will not be used
	// *********************************************************
	printlines();
	printf("Greedy Algorithm Setups\n");
	printlines();
	printf("Regularizer of cardinality in knapsack constraint = %f\n", alpha);
	printf("Regularizer of the threshold in the saturate coverage term = %f\n", beta);
	printf("Parameter of the softmax function = %f\n", gamma);
	printf("Total number of setences = %d, and percentage to be selected = %f\n",nUtt, per);
	printf("Submodular objective function: %s function\n", obj);
	printf("No diversity rewarding term will be applied...\n");
	printlines();
	printf("Start accelerated greedy algorithm\n");


	// initilize the priority queue
	printf("Initialize priority queue\n");
	for (i = 0; i < nV; i++) {
		// evaluate every i \in ground_set V
		if (!strcmp(obj, "facility"))
			sumV = (facility(graph,nV,preCompute,i))/pow(costList[i],alpha);
		if (!strcmp(obj, "saturate"))
			sumV = (coverage(graph,nV,preCompute,beta,threshV,i))/pow(costList[i],alpha);
		if (!strcmp(obj, "softmax"))
			sumV = (softmax(graph, nV, preCompute,i, gamma))/pow(costList[i],alpha);
		rho.push(Increment(sumV,i));
	}
	cout << endl;

	while (! rho.empty()) {
		topid = rho.top().get_index();
		rho.pop();
		if (!strcmp(obj, "facility"))
			maxV = facility(graph,nV,preCompute,topid);
		if (!strcmp(obj, "saturate"))
			maxV = coverage(graph,nV,preCompute,beta,threshV,topid);
		if (!strcmp(obj, "softmax"))
			maxV = softmax(graph, nV, preCompute, topid, gamma);

		newV = (maxV - preV)/pow(costList[topid],alpha);
		if (verbosity >= 5) printf("max gain = %.6e, rho->top() = %.6e\n",newV,rho.top().get_value());
		if (newV < rho.top().get_value()) {
			rho.push(Increment(newV, topid)); // if condition not satisfied, push back and re-sort
			sort_cnt++;
			if (verbosity >= 10) printf("Condition not met, re-sorting queue (%d iterations)\n",sort_cnt);
		}
		else {
			// guaranteed to be optimal because of submodularity
		    hashSelected[topid] = true;
            SelectedSet[nSelected++] = topid;
			objV += newV;
			currentCost += costList[topid];
			printf("---> Selecting %dth sample (%d selected, current normalized increament = %.6e), curCost/budget = %.6e/%.6e, preV = %.6e, curV = %.6e\n", topid, nSelected, newV,currentCost, maxCost, preV, maxV);
			preV = maxV;
			// update
			if (!strcmp(obj, "facility")){
		        for (i = 0; i < nV; i++) {
					if(graph[i].getWeight(topid)>preCompute[i]){
						preCompute[i]=graph[i].getWeight(topid);
					}
				}
			}
			if (!strcmp(obj, "saturate")){
		        for (i = 0; i < nV; i++) {
					preCompute[i]+=graph[i].getWeight(topid);
				}
			}
			if (!strcmp(obj, "softmax")){
				for (i = 0; i < nV; i++){
					preCompute[i] += exp(gamma*graph[i].getWeight(topid));
				}
			}
			sort_cnt = 0;
			if (currentCost >= maxCost)
				break;
		}
	}
	printlines();
	printf("The greedy selection is now completed\n");

    // done with all .. get total time for init.
    getrusage(RUSAGE_SELF,&rue);

    // print out time spent so far.
    reportTiming(rus,rue);

	// *********************************************************
	// output the selected list
	// *********************************************************
	ofp = fopen(strOutput,"w");
	for(i=0;i<nSelected;i++)
		fprintf(ofp,"%d ",SelectedSet[i]);

	fclose(ofp);
	return 0;

}
