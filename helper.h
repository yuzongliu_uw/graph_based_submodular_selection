/*
 * helper.h
 *
 *  Created on: Jan 27, 2013
 *      Author: Yuzong Liu (yzliu@ee.washington.edu)
 *
 *  This code contains various handlers and functions for the greedy selection.
 *
 */

#ifndef HELPER_H_
#define HELPER_H_

#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>
#include "stdlib.h"
#include "time.h"
#include <ctime>
using namespace std;

class Increment{
	public:
		Increment(){};
		Increment(double x, int i){ value=x; index=i; }
		bool operator< (const Increment&) const;

		int get_index() const { return index; }
		double get_value() const{ return value; }
	private:
		int index;
		double value;
};

bool Increment::operator< (const Increment& right) const
{
	return value < right.value;
}


void printlines(){
 	cout<< "----------------------------------------------\n";
 }


void printHelp() {

  cout << "----------------------------------------------\n";
  cout << "----------------------------------------------\n";

}

long GetFileSize(const char * filename )
{
    struct stat statebuf;

    if ( stat( filename, &statebuf ) == -1 )
        return -1L;
    else
        return statebuf.st_size;
}

void reportTiming(// input
                  const struct rusage& rus,
                  const struct rusage& rue) {

  struct timeval utime;
  double utimef;
  struct timeval stime;
  double stimef;

  /* user time */
  utime.tv_sec = rue.ru_utime.tv_sec - rus.ru_utime.tv_sec ;
  if ( rue.ru_utime.tv_usec < rus.ru_utime.tv_usec ) {
    utime.tv_sec--;
    utime.tv_usec = 1000000l - rus.ru_utime.tv_usec +
      rue.ru_utime.tv_usec;
  } else
    utime.tv_usec = rue.ru_utime.tv_usec -
      rus.ru_utime.tv_usec ;
  utimef = (double)utime.tv_sec + (double)utime.tv_usec/1e6;

  /* system time */
  stime.tv_sec = rue.ru_stime.tv_sec - rus.ru_stime.tv_sec ;
  if ( rue.ru_stime.tv_usec < rus.ru_stime.tv_usec ) {
    stime.tv_sec--;
    stime.tv_usec = 1000000l - rus.ru_stime.tv_usec +
      rue.ru_stime.tv_usec;
  } else
    stime.tv_usec = rue.ru_stime.tv_usec -
      rus.ru_stime.tv_usec ;

  stimef = (double)stime.tv_sec + (double)stime.tv_usec/1e6;
  printf("User: %f, System: %f, CPU %f\n", utimef, stimef, utimef+stimef);

}


#endif /* HELPER_H_ */
