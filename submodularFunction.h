/*
 * submodularFunction.h
 *
 *  Created on: Jan 27, 2013
 *      Author: Yuzong Liu (yzliu@ee.washington.edu)
 *
 *  This code contains a set of submodular functions for the subset selection
 *
 */

#ifndef SUBMODULARFUNCTION_H_
#define SUBMODULARFUNCTION_H_

#include <vector>
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "graph.h"
	 
#define SMALLEST_NUMBER -1e70
#define LARGEST_NUMBER 1e70
	 

double facility(node* graph, int nV, double* preMax, int newi){
// implementation of facility location function
	int i;
	double sum = 0;
	for(i = 0;i < nV; i++){
		double maxV = graph[i].getWeight(newi);
		if (preMax[i] > maxV)
			maxV = preMax[i];
		sum += maxV;
	}
	return sum;
		
}

double coverage(node* graph, int nV, double* preC, double beta, double* threshV, int newi){
// implementation of saturated coverage function
    int i;
    double sum = 0;
    for (i = 0; i < nV; i++) {
        if (preC[i] + graph[i].getWeight(newi) > beta * threshV[i])
            sum += beta * threshV[i];
        else
            sum += (preC[i]+graph[i].getWeight(newi));
    }
    return sum;
}

double softmax(node* graph, int nV, double* preCompte, int newi, double gamma) {
// implementation of softmax facility location function
	double sum = 0;
	for (int i = 0; i < nV; i++) {
		sum += log(1+preCompte[i]+exp(gamma*graph[i].getWeight(newi)))/gamma;
	}
	return sum;
}

double diversity(int** clusterID, double newReward, int nClusters, double** clusterReward, int topid, int nClusterMethod, vector<double> wgts, const char* funcName){
// implementation of diversity reward function
	double sum=0;
	for (int i = 0; i < nClusterMethod; i++) {
		for (int c = 0; c < nClusters; c++) {
			if (c!=clusterID[i][topid]) {			
				if (!strcmp(funcName,"sqrt"))
					sum += wgts[i]*sqrt(clusterReward[i][c]);
				else if (!strcmp(funcName,"log"))
					sum += wgts[i]*log(1+clusterReward[i][c]);
				else{
					printf("Invalid concave function, use sqrt function\n");
					sum += wgts[i]*sqrt(clusterReward[i][c]);
				}
			}
			else {
				if (!strcmp(funcName,"sqrt"))
					sum += wgts[i]*sqrt(clusterReward[i][c]+newReward);
				else if (!strcmp(funcName,"log"))
					sum += wgts[i]*sqrt(clusterReward[i][c]+newReward);
				else{
					printf("Invalid concave function, use sqrt function\n");
					sum += wgts[i]*sqrt(clusterReward[i][c]+newReward);
				}
			}
		}	
		if (sum > LARGEST_NUMBER) {
			printf("Overflown, exit now!");
			exit(-1);			
		}	
	}
	return sum;
}

#endif /* SUBMODULARFUNCTION_H_ */
